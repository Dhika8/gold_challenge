import re
from flask import Flask, jsonify
from Gold import clean
from flask import Flask, render_template
import pandas as pd
import os
import sqlite3 as sq

app = Flask(__name__)

from flask import request
from flasgger import Swagger, LazyString, LazyJSONEncoder
from flasgger import swag_from

app.json_encoder = LazyJSONEncoder
swagger_template = dict(
    info = {
        'title': LazyString(lambda: 'API Documentation for Data Processing and Modeling'),
        'version': LazyString(lambda: '1.0.0'),
        'description': LazyString(lambda: 'Dokumentasi API untuk Data Processing dan Modeling')
    },
    host = LazyString(lambda: request.host)
)
swagger_config = {
    "headers": [],
    "specs": [
        {
            "endpoint": 'docs',
            "route": '/docs.json'
        }
    ],
    "static_url_path": "/flasgger_static",
    "swagger_ui": True,
    "specs_route": "/docs/"
}
swagger = Swagger(app, template=swagger_template,config=swagger_config)

@swag_from("docs/cleaan.yml", methods=['POST'])
@app.route('/cleaan', methods=['POST'])
def cleaning():
    a = request.json['inputPilihanUtama']
    b = request.json['inputData']
    akhir = clean(a,b)
    sql_data = 'dbd_gold.db'
    conn = sq.connect(sql_data)
    cur = conn.cursor()

    cur.execute('''DROP TABLE IF EXISTS Goldd;''')
    akhir.to_sql('Goldd', conn, if_exists='replace', index=False)

    conn.commit()
    conn.close()
    print(akhir)
    jsonakhir = {'hasil' : list(akhir)}
    return(jsonify(jsonakhir))


if __name__ == '__main__':
    app.run()
