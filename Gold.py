def clean(a,b):
    import pandas as pd
    import re
    import nltk
    import Sastrawi
    from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory
    from nltk.tokenize import word_tokenize
    from nltk.probability import FreqDist
    from nltk.corpus import stopwords
    from io import StringIO
    nltk.download('punkt')
    nltk.download('wordnet')
    nltk.download('omw-1.4')
    nltk.download('stopwords')

    #a = 2
    #b = 'data.csv'
    #b = "Andi kerap melakukan transaksi rutin secara daring atau online anjing lu seks. Menurut Andi belanja online lebih praktis dan & murah.!$"
    ap = pd.read_csv('abusive.csv')
    stop_words = ap['ABUSIVE'].astype(str).values.tolist()
    f = lambda x: ' '.join(w for w in x if not w in stop_words)

    if a == "1":
        ac = b
    else: 
        ac = pd.read_csv(b,  encoding = 'latin')

    punctuation = re.compile(r'[^\w\s]')
    factory = StopWordRemoverFactory()
    stopword = factory.create_stop_word_remover()

    if a == "2":
        ac['Tweet'] = ac['Tweet'].apply(lambda x:x.lower()) 
        ac['Kata-Kata Dalam Tweet'] = ac['Tweet'].apply(lambda x:re.sub(punctuation, r' ', str(x)))
        ac['Kata-Kata Dalam Tweet'] = ac['Kata-Kata Dalam Tweet'].apply(nltk.tokenize.word_tokenize)
        ac['Tweet_Clean'] = ac['Kata-Kata Dalam Tweet'].apply(f)
        ac['Tweet_Clean'] = ac['Tweet_Clean'].apply(lambda x:re.sub(punctuation, r' ', str(x)))
        ac['Tweet_Cleann'] = ac['Tweet_Clean'].apply(nltk.tokenize.word_tokenize)
        ac['Banyak Kata Dalam Tweet'] = ac['Kata-Kata Dalam Tweet'].apply(lambda x: len(str(x).split(',')))
        ac['Banyak Kata Dalam Tweet_Clean'] = ac['Tweet_Cleann'].apply(lambda x: len(str(x).split(',')))
        ac.drop(['Tweet_Cleann','Kata-Kata Dalam Tweet'], axis=1, inplace=True)
        ac
        print(ac)
    else:
        ac = ac.lower()
        ac = pd.DataFrame({'Data': [ac]})
        ac['Data_Clean'] = ac['Data'].apply(lambda x:re.sub(punctuation, r' ', str(x)))
        ac['Data_Clean3'] = ac['Data_Clean'].apply(nltk.tokenize.word_tokenize)
        ac['Jumlah Kata Data'] = ac['Data_Clean3'].apply(lambda x: len(str(x).split(',')))
        ac['Data_Clean1'] = ac['Data_Clean3'].apply(f)
        ac['Data_Clean1'] = ac['Data_Clean1'].apply(lambda x:re.sub(punctuation, r' ', str(x)))
        ac['Data_Clean2'] = ac['Data_Clean1'].apply(nltk.tokenize.word_tokenize)
        ac['Jumlah Kata Data1'] = ac['Data_Clean2'].apply(lambda x: len(str(x).split(',')))
        #ad = dp[['Data','Data_Clean','Data_Clean1','Jumlah Kata Data','Jumlah Kata Data1']]
        ac.drop(['Data_Clean3','Data_Clean2'], axis=1, inplace=True)
        print(ac)

    return ac